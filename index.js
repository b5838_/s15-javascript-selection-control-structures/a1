/*  3.) Prompt the user for 2 numbers and perform different arithmetic operations based on the total of the two numbers:
        If the total of the two numbers is less than 10, add the numbers
        If the total of the two numbers is 10 - 20, subtract the numbers
        If the total of the two numbers is 21 - 29 multiply the numbers
        If the total of the two numbers is greater than or equal to 30, divide the numbers
    4. Use an alert for the total of 10 or greater and a console warning for the total of 9 or less.
*/
let num1 = parseInt(prompt("Provide a Number"));
let num2 = parseInt(prompt("Provide another Number"));

let total = num1 + num2;

if(total < 10) {
    let sum = num1 + num2;
    console.warn("The sum of two numbers: " + sum);
}
else if(total >= 10 && total <=20){
    let difference = num2 - num1
    alert("The difference of two numbers: " + difference);
}
else if(total >= 21 && total <=29){
    let product = num1 * num2
    alert("The product of two numbers: " + product);
}
else if(total >= 30){
    let quotient = num2 / num1;
    alert("The quotient of two numbers: " + quotient);
}

// 5. Prompt the user for their name and age and print out different alert messages based on the user input:
    // a.If the name OR age is blank(“”), print the message are you a time traveler?
    // b. If the name AND age is not blank, print the message with the user’s name and age.
    // 6. Create a function named isLegalAge which will check if the user's input from the previous prompt is of legal age:
        // 18 or greater, print an alert message saying You are of legal age.
        // 17 or less, print an alert message saying You are not allowed here.

let name = prompt("What is your name?");
let age = parseInt(prompt("What is your age?"));

if(name == "" || age == ""){
    console.log("Are you a time traveler?");
} 
else if(name != "" && age != ""){
    console.log("Hello " + name + ". Your age is " + age);
}

function isLegalAge(){
    if(age >= 18){
        return "You are of legal age.";
    }
    else{
        return "You are not allowed here.";
    }
}
alert(isLegalAge());

// 7. Create a switch case statement that will check if the user's age input is within a certain set of expected input:
    // 18 - print the message You are now allowed to party.
    // 21 - print the message You are now part of adult society.
    // 65 - print the message We thank you for your contribution to society.
    //  Any other value - print the message Are you sure you're not an alien?

switch (age){
    case (age >= 18) :
        console.log("You are now allowed to party.");
        break;
    case (age >= 21) :
            console.log("You are now part of adult society");
            break;
    case (age >= 65) :
        console.log("We thank you for your contribution society.");
        break;
    default :
        console.log("Are you sure you're not an alien?");
}

// 8. Create a try catch finally statement to force an error, print the error message as a warning and use the function isLegalAge to print alert another message.

try{
    alert(isLegalAge());
}
catch(error){
    console.warn(error.age);
}
finally{
        alert("another message.");
}



// ***** CORRECT ANSWER by Sir Angelito ***** 
/*
FOR 3 & 4;

let num1 = parseInt(prompt("Provide a Number"));
let num2 = parseInt(prompt("Provide another Number"));
let total = num1 + num2;

    if(total < 10) {
        console.warn("The sum of two numbers: " + total);
    }
    else if(total >= 10 && total <=20){
        alert("The difference of two numbers: " + (num2 - num1));
    }
    else if(total >= 21 && total <=29){
        alert("The product of two numbers: " + (num1 * num2));
    }
    else if(total >= 30){
        alert("The quotient of two numbers: " + {(num2 / num1)});
    }
*/

// ----------------------------------

/*
FOR 5;

    let name = prompt("What is your name?");
    let age = prompt("What is your age?");

    if (name != "" && age != ""){
        alert("Hello " + name + ". Your age is " + age)
    }
    else {
        alert("Are you a time traveler?");
    }
    */

// ---------------------------------

/*
FOR 6;

    function isLegalAge(age){
        if(age >= 18){
            return "You are of legal age.";
        }
        else{
            return "You are not allowed here.";
        }
    }
    alert(isLegalAge(age));
*/

// ---------------------------------

/*
FOR 7;

    switch (age){
        case 18 :
            alert("You are now allowed to party.");
            break;
        case 21 :
                alert("You are now part of adult society");
                break;
        case 65 :
            alert("We thank you for your contribution society.");
            break;
        default :
            alert("Are you sure you're not an alien?");
    }
*/

// ------------------------------


/*
FOR 8;

    try{
        alert(isLegalAge);
    }
    catch (error){
        console.warn(error.message);
    }
    finally{
        alert("This is a test.")
    }
*/
